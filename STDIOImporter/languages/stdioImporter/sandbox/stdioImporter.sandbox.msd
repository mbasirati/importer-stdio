<?xml version="1.0" encoding="UTF-8"?>
<solution name="stdioImporter.sandbox" uuid="d6b5ad5f-0ac9-4be4-aa79-33615f4f138c" compileInMPS="true">
  <models>
    <modelRoot contentPath="${module}" type="default">
      <sourceRoot location="models" />
    </modelRoot>
  </models>
  <sourcePath />
  <dependencies>
    <dependency reexport="false">~_PreferencesModule#STDIOImporter.__spreferences.PlatformTemplates(STDIOImporter.__spreferences.PlatformTemplates)</dependency>
  </dependencies>
  <usedLanguages>
    <usedLanguage>017fba0e-af15-4a23-b0a8-02b5c1141e75(com.mbeddr.cc.var.annotations)</usedLanguage>
    <usedLanguage>e401b447-8019-4ccd-a72c-bfb0230f5782(com.mbeddr.cc.var.fm)</usedLanguage>
    <usedLanguage>ceab5195-25ea-4f22-9b92-103b95ca8c0c(jetbrains.mps.lang.core)</usedLanguage>
    <usedLanguage>4552c9e5-a342-4299-8de1-338d8e5679fc(stdioImporter)</usedLanguage>
  </usedLanguages>
  <usedDevKits>
    <usedDevKit>0ca77142-1eea-4b14-b369-69bdaa1c44fb(com.mbeddr.analyses.core)</usedDevKit>
  </usedDevKits>
</solution>

