package stdio_parser;

/**
 * Created by basirati on 8/2/14.
 */
public class Typedef extends Declaration{

    private String as;

    public String getAs() {
        return as;
    }

    public void setAs(String as) {
        this.as = as;
    }

}
